import Vue from 'vue'

Vue.directive('long-press', {
  bind: function (el, binding) {
    console.log(this)
    var self = {}

    self._timeout   = null
    self._onmouseup = function () {
      clearTimeout(self._timeout)
    }

    self._onmousedown = function (e) {
      var context = this

      self._timeout = setTimeout(function () {
        binding.value(context, e)
      }, 250)
    }

    el.addEventListener('mousedown', self._onmousedown)
    document.addEventListener('mouseup', self._onmouseup)
  }
})

Vue.prototype.$curryMethod = function (methodName, ...argsFromTemplate) {
  return (...argsFromDirective) => this[methodName](this, ...argsFromTemplate, ...argsFromDirective)
}
