import { Validator } from 'vee-validate'
import { UniqueUsername } from '@/api-services/validation.service'

Validator.extend('uniqueUsername', {
  validate  : (value) => {
    return UniqueUsername(value).then(function (response) {
      return {
        valid: response.data.valid,
        data : {
          message: response.data.message
        }
      }
    })
  },
  getMessage: (field, params, data) => {
    return data.message
  }
})

export default Validator
