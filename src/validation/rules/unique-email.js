import { Validator } from 'vee-validate'
import { UniqueEmail } from '@/api-services/validation.service'

Validator.extend('uniqueEmail', {
  validate  : (value) => {
    return UniqueEmail(value).then(function (response) {
      return {
        valid: response.data.valid,
        data : {
          message: response.data.message
        }
      }
    })
  },
  getMessage: (field, params, data) => {
    return data.message
  }
})

export default Validator
