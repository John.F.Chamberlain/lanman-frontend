import UniqueEmail from './rules/unique-email'
import UniqueUsername from './rules/unique-username'

export default {
  UniqueEmail,
  UniqueUsername
}
