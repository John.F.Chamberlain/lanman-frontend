import Axios from 'axios'

const RESOURCE_NAME = '/validate'

export function UniqueEmail ($email) {
  return Axios.post(`${RESOURCE_NAME}/email`, { email: $email })
}

export function UniqueUsername ($username) {
  return Axios.post(`${RESOURCE_NAME}/username`, { username: $username })
}
