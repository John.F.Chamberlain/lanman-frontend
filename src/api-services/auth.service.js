import Axios from 'axios'

const RESOURCE_NAME = '/auth'

export function ProcessRegistration ($form) {
  return Axios.post(`${RESOURCE_NAME}/register`, $form)
}

export function ProcessLogin ($form) {
  return Axios.post(`${RESOURCE_NAME}/login`)
}
