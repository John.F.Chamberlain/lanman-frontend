import Axios from 'axios'

const RESOURCE_NAME = '/app'

export default {
  getSettings () {
    return Axios.get(`${RESOURCE_NAME}/settings`)
  }
}
