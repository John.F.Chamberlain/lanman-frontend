import Vue from 'vue'
import Router from 'vue-router'
import PageHome from '@/components/Pages/PageHome'
import RegistrationForm from '@/components/Auth/RegistrationForm'

Vue.use(Router)

const routes = [
  {
    path     : '/',
    name     : 'Home',
    component: PageHome,
    meta     : {
      title: 'Home'
    }
  },
  {
    path     : '/auth/register',
    name     : 'auth.register',
    component: RegistrationForm,
    meta     : {
      title: 'Register'
    }
  }
]

const router = new Router({
  routes

})

router.beforeEach((to, from, next) => {
  console.log(to)
  if (to.meta && to.meta.title) {
    document.title = to.meta.title + ' :: ' + process.env.APP_TITLE
  } else {
    document.title = process.env.APP_TITLE
  }

  Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el))

  if (!to.meta || !to.meta.metaTags) return next()

  to.meta.metaTags.map(tagDef => {
    const tag = document.createElement('meta')

    Object.keys(tagDef).forEach(key => {
      tag.setAttribute(key, tagDef[key])
    })
    tag.setAttribute('data-vue-router-controlled', '')
    return tag
  })
    .forEach(tag => document.head.appendChild(tag))

  next()
})

export default router
