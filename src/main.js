import Vue from 'vue'
import App from './App'
import router from './router'
import 'bootstrap'
import './assets/theme'
import Axios from 'axios'
import BootstrapVue from 'bootstrap-vue'
import VeeValidate from 'vee-validate'
import VuePusher from 'vue-pusher'
import Echo from 'laravel-echo'
import './assets/scss/app.scss'
import VueLodash from 'vue-lodash'

Vue.config.productionTip = false

Axios.defaults.baseURL                            = process.env.API_ENDPOINT
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
Axios.defaults.headers.common['X-CSRF-TOKEN']     = document.head.querySelector('meta[name="csrf-token"]').content

window.Pusher = require('pusher-js')

window.Echo = new Echo({
  broadcaster: 'pusher',
  key        : process.env.PUSHER_APP_KEY,
  cluster    : process.env.PUSHER_CLUSTER,
  encrypted  : process.env.PUSHER_ENCRYPT
})

Vue.use(BootstrapVue)
Vue.use(VueLodash, { name: 'lodash' })
Vue.use(VeeValidate, {
  inject       : true,
  fieldsBagName: 'veeFields'
})

Vue.use(VuePusher, {
  api_key: process.env.PUSHER_APP_KEY,
  options: {
    cluster  : process.env.PUSHER_CLUSTER,
    encrypted: (typeof process.env.PUSHER_ENCRYPT === 'undefined' || process.env.PUSHER_ENCRYPT === null) ? true : process.env.PUSHER_ENCRYPT
  }
})

/* eslint-disable no-new */
new Vue({
  el        : '#app',
  router,
  components: { App },
  template  : '<App/>'
})
