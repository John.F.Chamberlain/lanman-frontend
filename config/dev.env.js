'use strict'
const merge   = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV          : '"development"',
  API_ENDPOINT      : '"http://localhost:8000/api"',
  PUSHER_APP_KEY    : '"ba7f65c65db243118720"',
  PUSHER_CLUSTER    : '"us2"',
  PUSHER_ENCRYPT    : 'true',
  STRIPE_PUBLIC     : '""',
  RECAPTCHA_PUBLIC  : '""',
  GOOGLE_OAUTH_URL  : '""',
  FACEBOOK_OAUTH_URL: '""',
})
