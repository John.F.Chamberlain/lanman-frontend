'use strict'
module.exports = {
  NODE_ENV          : '"production"',
  API_ENDPOINT      : '"https://lanman.moodusit.com/api"',
  APP_TITLE         : '"ATRLAN"',
  PUSHER_APP_KEY    : '""',
  PUSHER_CLUSTER    : '""',
  PUSHER_ENCRYPT    : 'true',
  STRIPE_PUBLIC     : '""',
  RECAPTCHA_PUBLIC  : '""',
  GOOGLE_OAUTH_URL  : '""',
  FACEBOOK_OAUTH_URL: '""',
}
